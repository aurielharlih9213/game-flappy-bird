﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Pipe : MonoBehaviour
{
	[SerializeField] private Bird bird;
	[SerializeField] private float speed = 1;
	[SerializeField] private GameObject bulletPrefabs;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    	// cek jika burung belum mati
    	if (!bird.IsDead()) {
    		// Membuat Pipa bergerak ke arah kiri dengan kecepatan tertentu
    		transform.Translate(Vector3.left * speed * Time.deltaTime, Space.World);
    	}
    }

    // Membuat bird mati jika tersentuuh dan menjatuhkan ke ground
    private void OnCollisionEnter2D(Collision2D collision)
    {
    	Bird bird = collision.gameObject.GetComponent<Bird>();

    	// Pengecekan null value
    	if (bird) {
    		// Mendapat component collider pada game object
    		Collider2D collider = GetComponent<Collider2D>();

    		// melakukan pengecekan null
    		if (collider) {
    			// Menonaktifkan collider
    			collider.enabled = false;
    		}

    		// Burung mati
    		bird.Dead();
    	}
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
    	Collider2D collider = GetComponent<Collider2D>();
    	Destroy(collision.gameObject);
    	Destroy(collider.gameObject);
    }
}
