﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Bird : MonoBehaviour
{
	[SerializeField] private float upForce = 100;
	[SerializeField] private bool isDead;
	[SerializeField] private UnityEvent OnJump, OnDead, OnAddPoint;
	[SerializeField] private int score;
	[SerializeField] private Text scoreText;
	[SerializeField] private GameObject bulletPrefabs;
	[SerializeField] private float bulletSpeed = 1.0f;

	private Rigidbody2D rigidBody2D;

	private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        // ambil komponen
        rigidBody2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        // pengecekan jika burung belum mati dan tekan mouse kiri
        if (!isDead && Input.GetMouseButtonDown(0))
        {
        	// Loncat
        	Jump();
        }

        // Cek jika burung belum mati dan tekan mouse kanan
        if (!isDead && Input.GetMouseButtonDown(1))
        {
        	// Tembak
        	Shoot();
        }
    }

    // Fungsi cek mati atau tidak
    public bool IsDead()
    {
    	return isDead;
    }

    // Fungsi untuk mati
    public void Dead()
    {
    	// Cek jika belum mati dan value OnDead tidak null
    	if (!isDead && OnDead != null)
    	{
    		// Panggil semua event OnDead
    		OnDead.Invoke();
    	}

    	// Mengubah variabel isDead ke true
    	isDead = true;
    }

    // Fungsi Lompat
    void Jump()
    {
    	// cek ada tidak rigidbody
    	if (rigidBody2D)
    	{
    		// kecepatan burung berhenti saat jatuh
    		rigidBody2D.velocity = Vector2.zero;

    		// menambah gaya ke arah sumbu y agar burung loncat
    		rigidBody2D.AddForce(new Vector2(0, upForce));
    	}

    	// pengecekan null variabel
    	if (OnJump != null)
    	{
    		// Jalankan semua event OnJump
    		OnJump.Invoke();
    	}
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
    	// hentikan animasi
    	animator.enabled = false;
    	
    }

    public void AddScore(int value)
    {
    	// menambah score
    	score += value;

    	// cek null value
    	if (OnAddPoint != null) {
    		// panggil semua event OnAddPoint
    		OnAddPoint.Invoke();
    	}

    	// Mengubah tampilan scoreText
    	scoreText.text = score.ToString();
    }

    public void Shoot()
    {
    	GameObject bullet = Instantiate(bulletPrefabs, transform.position, Quaternion.identity);
    	bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(bulletSpeed, 0.0f);
    }
}
